function BetAssistant(sceneAssistant, callbackFunc, playerMoney, currentBet) {
	this.callbackFunc = callbackFunc;
	this.sceneAssistant = sceneAssistant;
	this.controller = sceneAssistant.controller;
	this.playerMoney = playerMoney;
	this.currentBet = currentBet;
	this.lastTextValue = currentBet;
}

BetAssistant.prototype.setup = function(widget) {
	$('maxBet').update(this.playerMoney);
	$('currentMoney').update(Math.abs(this.playerMoney).toFixed(2));
	this.widget = widget;
	
	var attributes = {
		hintText: '',
		textFieldName: 'name', 
		modelProperty: 'original', 
		multiline: false,
		disabledProperty: 'disabled',
		focus: true, 
		modifierState: Mojo.Widget.numLock,
		limitResize: false, 
		holdToEnable: false, 
		focusMode: Mojo.Widget.focusSelectMode,
		changeOnKeyPress: true,
		textReplacement: false,
		maxLength: 30,
		requiresEnterKey: false,
		charsAllow: this.checkCharacter
	};

	this.model = {
		original: Math.min(this.currentBet, this.playerMoney),
		disabled: false
	};

	this.controller.setupWidget('textField', attributes, this.model);

	this.save = this.save.bind(this);
	Mojo.Event.listen(this.controller.get('continue'), Mojo.Event.tap, this.save);
};

BetAssistant.prototype.save = function(event) {
	var amount = this.model.original;
	var amountNum = parseFloat(amount);
	if (amountNum < MINIMUM_BET || isNaN(amount)) {
		amountNum = MINIMUM_BET;
	}
	this.callbackFunc(amountNum);
	this.widget.mojo.close();
};

BetAssistant.prototype.cancel = function(event) {
	this.widget.mojo.close();
};

BetAssistant.prototype.checkCharacter = function(characterEntered) {
	return characterEntered >= 48 && characterEntered <= 57; // ascii chars between 0 and 9 allowed
};

BetAssistant.prototype.activate = function(event) {};

BetAssistant.prototype.deactivate = function(event) {};

BetAssistant.prototype.cleanup = function(event) {
	Mojo.Event.stopListening(this.controller.get('continue'), Mojo.Event.tap, this.save);  
};