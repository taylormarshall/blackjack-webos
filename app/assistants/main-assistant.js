function MainAssistant() {
    // TODO consider saving entire game state
    this.player = new Player();
    this.bettingBox = new BettingBox(this.player);
    this.DEALER_DELAY_TIME = 0.5; //seconds
    this.deck = new Deck(SETTINGS.numberOfDecks);
    this.round = new Round(this.deck);
    this.buttons = {
        hit: { label: 'Hit',command: 'hit', width: 150 },
        stand: { label: 'Stand', command: 'stand', width: 150 },
        doubleDown: { label: 'Double Down', command: 'doubledown' },
        split: { label: 'Split', command: 'split', disabled: true },
        surrender: { label: 'Surrender', command: 'surrender' }
    };
    this.commandMenuModel = { items: [this.buttons.hit, this.buttons.stand] };
    this.viewMenuModel = { items: [this.buttons.doubleDown, this.buttons.split, this.buttons.surrender] };
    this.appMenuModel = {
        items: [
            Mojo.Menu.editItem,
            { label: 'New Game', command: 'new-game' },
            Mojo.Menu.prefsItem,
            Mojo.Menu.helpItem
        ],
        visible: true
    };
    this.doDealerTurn = this.doDealerTurn.bind(this);
    this.setPlayerBet = this.setPlayerBet.bind(this);
    this.changeBet = this.changeBet.bind(this);
}

MainAssistant.prototype.setup = function() {
    $$('body > *').each(function(element) {
        element.addClassName('tableSurface');
    });
    $('money').update('$' + this.player.money.toFixed(2));
    $('points').update('0');
    $('bet').update('$' + this.player.betAmount.toFixed(2));
    this.playerSections = $$('#playerSection > div');
    this.dealerSection = $('dealerSection').childElements()[0];
    this.controller.setupWidget(Mojo.Menu.commandMenu, undefined, this.commandMenuModel);
    this.controller.setupWidget(Mojo.Menu.viewMenu, undefined, this.viewMenuModel);
    this.controller.setupWidget(Mojo.Menu.appMenu, { omitDefaultItems: true }, this.appMenuModel);

    // TODO: only show "change bet" if they aren't resuming a saved game
    // or if they are at a point in a saved game where it makes sense
    this.changeBet(); // let player pick the bet initially
};

MainAssistant.prototype.activate = function(event) {};
MainAssistant.prototype.deactivate = function(event) {};
MainAssistant.prototype.cleanup = function(event) {};

MainAssistant.prototype.handleCommand = function(event) {
    if (event.type === Mojo.Event.commandEnable &&
        (event.command === Mojo.Menu.helpCmd || event.command === Mojo.Menu.prefsCmd)) {
        event.stopPropagation(); // enables help so we can handle it
    }
    if (event.type === Mojo.Event.command) {
        switch (event.command) {
            case 'palm-show-app-menu':
                // user opened app menu, don't do anything
                break;
            case Mojo.Menu.prefsCmd:
                this.controller.stageController.pushScene('preferences');
                break;
            case Mojo.Menu.helpCmd:
                this.controller.stageController.pushScene('support');
                break;
            case 'hit':
                this.playerHit();
                break;
            case 'stand':
                this.playerStand();
                break;
            case 'surrender':
                this.playerSurrender();
                break;
            case 'doubledown':
                this.playerDoubleDown();
                break;
            case 'split':
                this.playerSplit();
                break;
            case 'new-game':
                this.player.reset();
                this.changeBet();
                break;
            default:
                Mojo.Controller.errorDialog("Got unexpected command: " + event.command);
                break;
        }
    }
    if (event.command && event.command.slice(0, 5) !== 'palm-') {
        // non-menu command, need to update the display
        this.updateDisplay();
    }
};

// TODO this might be better as a wrapper/decorator
MainAssistant.prototype.playDrawCardSound = function() {
    if (SETTINGS.soundEnabled) {
        try {
            this.controller.serviceRequest('palm://com.palm.audio/systemsounds', {
                method: 'playFeedback',
                parameters: { name: 'pageforward_01' },
                onFailure: null
            });
        } catch (e) {
            Mojo.Log.error("Error playing sound: ", e);
        }
    }
};

MainAssistant.prototype.updateDisplay = function() {
    var disableAllButtons = this.round.isPlayerFinished();
    this.updateButtons(disableAllButtons);

    this.playerSections.each(function(element) {
        element.update('');
    });
    this.dealerSection.update('');

    var dCards = this.round.dealerHand.cards;
    var pCards = [];
    var i;
    for (i = 0; i < this.round.playerHands.length; i++) {
        pCards.push(this.round.playerHands[i].cards);
    }

    for (i = 0; i < dCards.length; i++) {
        if (i === 1 && this.round.isHoleCardCovered) {
            this.dealerSection.insert(HOLECARD_TEMPLATE.evaluate({ color: SETTINGS.cardColor }));
        } else {
            this.dealerSection.insert(dCards[i].getHtml());
        }
    }

    for (i = 0; i < pCards.length; i++) {
        for (var j = 0; j < pCards[i].length; j++) {
            var section = this.playerSections[i];
            section.insert(pCards[i][j].getHtml());
            if (i === this.round.currentPlayerHandIndex && this.round.hasPlayerSplit()) {
                section.addClassName('currentHand');
            } else {
                section.removeClassName('currentHand');
            }
        }
    }
    $('money').update('$' + this.player.money.toFixed(2));
    $('points').update(this.round.getPlayerPoints());
    $('bet').update('$' + this.player.betAmount.toFixed(2));
};

MainAssistant.prototype.playerOutOfMoney = function() {
    this.controller.showAlertDialog({
        title: $L('Out of money!'),
        message: $L('You do not have enough money to make the minimum bet!'),
        preventCancel: true,
        allowHTMLMessage: true,
        choices: [{ label: $L('Start Over'), value: "startover" }],
        onChoose: this.changeBet
    });
    this.player.setMoney(500);
    this.player.changeBet(MINIMUM_BET);
};

MainAssistant.prototype.changeBet = function() {
    if (this.player.money < MINIMUM_BET) {
        this.player.changeBet(MINIMUM_BET);
    }
    this.getPlayerBet();
};

MainAssistant.prototype.dealNewHand = function() {
    if (this.player.money < MINIMUM_BET) {
        this.playerOutOfMoney();
    } else if (this.player.money < this.player.originalBet) {
        this.changeBet();
    } else {
        this.player.betAmount = this.player.originalBet;
        this.round = new Round(this.deck);
        this.bettingBox.newRound();
        this.updateDisplay();
    }
};

MainAssistant.prototype.getPlayerBet = function() {
    this.controller.showDialog({
        template: 'main/bet-scene',
        assistant: new BetAssistant(this, this.setPlayerBet, this.player.money, this.player.originalBet),
        preventCancel: true
    });
};

MainAssistant.prototype.setPlayerBet = function(newBet) {
    this.player.changeBet(newBet);
    this.controller.getSceneScroller().scrollTo(0, 0);
    this.dealNewHand();
};

MainAssistant.prototype.playerHit = function() {
    this.playDrawCardSound();
    this.round.playerHit();
    var points = this.round.getPlayerPoints();
    if (points > 21) { //player busted
        this.playerStand();
    }
};

MainAssistant.prototype.playerDoubleDown = function() {
    this.playDrawCardSound();
    this.bettingBox.doubleDown(this.round.currentPlayerHandIndex);
    this.round.playerDoubleDown();
    // player is not allowed to double after split
    // therefore we don't have to check for split hands here
    this.doDealerTurn.delay(this.DEALER_DELAY_TIME);
};

MainAssistant.prototype.playerStand = function() {
    this.round.playerStand();
    if (this.round.isPlayerFinished()) {
        this.doDealerTurn.delay(this.DEALER_DELAY_TIME);
    }
};

MainAssistant.prototype.setCurrentHand = function() {
    $('points').update(this.round.getPlayerPoints());
    this.playerSections[this.round.currentPlayerHandIndex].addClassName('currentHand');
};

MainAssistant.prototype.playerSplit = function() { // TODO consider holding "inactive" split hands in a totally different area
    this.playDrawCardSound();
    this.bettingBox.addBet();
    this.round.playerSplit();
};

MainAssistant.prototype.playerSurrender = function() {
    var moneyLost = this.bettingBox.surrender(this.round.currentPlayerHandIndex);
    this.showGameResult('You surrendered.<br/><br/> You lost $' + moneyLost.toFixed(2) + ' on this hand.');
};

MainAssistant.prototype.doDealerTurn = function() {
    if (this.round.isDealerFinished()) {
        this.doHandFinished();
    } else {
        this.playDrawCardSound();
        this.round.doNextDealerAction();
        this.updateDisplay();
        this.doDealerTurn.delay(this.DEALER_DELAY_TIME);
    }
};

MainAssistant.prototype.doHandFinished = function() {
    var result = '';
    var outcomes = this.round.getOutcomes();
    for (var i = 0; i < outcomes.length; i++) {
        var outcome = outcomes[i];
        result += outcome.playerScore + '-' + outcome.dealerScore + ': ';
        switch (outcome.winner) {
            case 'push':
                result += 'The hand was a push';
                break;
            case 'player':
                if (outcome.winnerBlackjack) {
                    result += 'Blackjack! ';
                } else if (outcome.loserBusted) {
                    result += 'Dealer busted. ';
                }
                result += 'You win.';
                break;
            case 'dealer':
                if (outcome.loserBusted) {
                    result += 'You busted.';
                } else {
                    result += 'Dealer wins.';
                }
                break;
        }
        result += '<br/>';
    }
    result += '<br/>';
    var winnings = this.bettingBox.payout(outcomes);
    if (winnings !== 0) {
        if (winnings > 0) {
            result += 'You won $';
        } else {
            result += 'You lost $';
        }
        result += Math.abs(winnings).toFixed(2) + ' on this hand.';
    } else {
        result += 'You broke even on this hand.';
    }
    $('money').update('$' + this.player.money.toFixed(2));
    this.showGameResult(result);
};

MainAssistant.prototype.startNewHand = function(playerChoice) {
    if (playerChoice === 'changebet') {
        this.changeBet();
    } else {
        this.dealNewHand();
    }
};

MainAssistant.prototype.showGameResult = function(msg) {
    // TODO  this would be better if it was not an alert
    var options = [{
        label: $L('Continue'),
        value: "continue",
        type: 'affirmative'
    }];
    if (this.player.money >= this.player.originalBet) {
        options.push({
            label: $L('Change Bet'),
            value: "changebet",
            type: 'negative'
        });
    }
    this.controller.showAlertDialog({
        onChoose: this.startNewHand,
        title: $L('Results for this hand:'),
        message: $L(msg),
        preventCancel: true,
        allowHTMLMessage: true,
        choices: options
    });
};

MainAssistant.prototype.updateButtons = function(disableAll) {
    var hasMoneyToDouble = this.bettingBox.canPlayerAffordDouble(this.round.currentPlayerHandIndex);
    this.buttons.split.disabled = !hasMoneyToDouble || !this.round.canPlayerSplit() || disableAll;
    this.buttons.surrender.disabled = !this.round.canPlayerSurrender() || disableAll;
    this.buttons.doubleDown.disabled = !hasMoneyToDouble || !this.round.canPlayerDoubleDown() || disableAll;
    this.buttons.stand.disabled = disableAll; //stand and hit are allowed if anything is enabled
    this.buttons.hit.disabled = disableAll;
    this.controller.modelChanged(this.viewMenuModel, this);
    this.controller.modelChanged(this.commandMenuModel, this);
};