function PreferencesAssistant() {
	/* this is the creator function for your scene assistant object. It will be passed all the
	   additional parameters (after the scene name) that were passed to pushScene. The reference
	   to the scene controller (this.controller) has not be established yet, so any initialization
	   that needs the scene controller should be done in the setup function below. */
    this.appMenuModel = {
        items: [Mojo.Menu.editItem, Mojo.Menu.helpItem],
        visible: true
    };
    this.cardBackColorModel = { value: SETTINGS.cardColor };
    this.soundEnabledModel = { value: SETTINGS.soundEnabled };
    this.numberOfDecksModel = { value: SETTINGS.numberOfDecks };
    this.cardBackColorAttrs = {
        trueLabel: "Blue",
        trueValue: 'blue',
        falseLabel: "Red",
        falseValue: 'red'
    };
    this.numberOfDecksAttrs = {
        choices: [
            { label: "One", value: 1 },
            { label: "Two", value: 2 },
            { label: "Three", value: 3 },
            { label: "Four", value: 4 },
            { label: "Five", value: 5 },
            { label: "Six", value: 6 },
            { label: "Seven", value: 7 },
            { label: "Eight", value: 8 },
            { label: "Nine", value: 9 }
        ],
        label: "Number of decks",
        labelPlacement: Mojo.Widget.labelPlacementLeft
    };
    this.soundToggled = this.soundToggled.bind(this);
    this.cardColorToggled = this.cardColorToggled.bind(this);
    this.numberOfDecksSelected = this.numberOfDecksSelected.bind(this);
}

PreferencesAssistant.prototype.setup = function() {
	/* this function is for setup tasks that have to happen when the scene is first created */

	/* use Mojo.View.render to render view templates and add them to the scene, if needed. */

	/* setup widgets here */
	this.controller.setupWidget(Mojo.Menu.appMenu, { omitDefaultItems: true }, this.appMenuModel);
	this.controller.setupWidget('soundEnabled', {}, this.soundEnabledModel);
    this.controller.setupWidget('cardBackColor', this.cardBackColorAttrs, this.cardBackColorModel);
    this.controller.setupWidget('numberOfDecks', this.numberOfDecksAttrs, this.numberOfDecksModel);

	/* add event handlers to listen to events from widgets */
	Mojo.Event.listen(this.controller.get('soundEnabled'), Mojo.Event.propertyChange, this.soundToggled);
    Mojo.Event.listen(this.controller.get('cardBackColor'), Mojo.Event.propertyChange, this.cardColorToggled);
    Mojo.Event.listen(this.controller.get('numberOfDecks'), Mojo.Event.propertyChange, this.numberOfDecksSelected);
};

PreferencesAssistant.prototype.activate = function(event) {};
PreferencesAssistant.prototype.deactivate = function(event) {};
PreferencesAssistant.prototype.cleanup = function(event) {};

PreferencesAssistant.prototype.handleCommand = function(event) {
	if (event.type == Mojo.Event.commandEnable &&
	    (event.command == Mojo.Menu.helpCmd)) {
         event.stopPropagation(); // enable help so we can handle it
    }
	if (event.type === Mojo.Event.command && event.command === Mojo.Menu.helpCmd) {
		Mojo.Controller.stageController.pushScene('support');
	}
};

PreferencesAssistant.prototype.soundToggled = function(event) {
    SETTINGS.soundEnabled = event.model.value;
    SETTINGS.save();
};

PreferencesAssistant.prototype.cardColorToggled = function(event) {
    SETTINGS.cardColor = event.model.value;
    SETTINGS.save();
    var holeCard = this.controller.get('holeCard');
    if (holeCard) {
        holeCard.src = new Template(HOLECARD_IMG_SRC).evaluate({ color: SETTINGS.cardColor });
    }
};

PreferencesAssistant.prototype.numberOfDecksSelected = function(event) {
    SETTINGS.numberOfDecks = event.model.value;
    SETTINGS.save();
};
