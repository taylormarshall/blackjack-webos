function RulesAssistant() {
	this.appMenuModel = { 
        items: [Mojo.Menu.editItem, Mojo.Menu.helpItem],
        visible: true
    };
}

RulesAssistant.prototype.setup = function() {
	this.controller.setupWidget(Mojo.Menu.appMenu, { omitDefaultItems: true }, this.appMenuModel);
};

RulesAssistant.prototype.activate = function(event) {};
RulesAssistant.prototype.deactivate = function(event) {};
RulesAssistant.prototype.cleanup = function(event) {};

RulesAssistant.prototype.handleCommand = function(event) {
	if (event.type == Mojo.Event.commandEnable &&
	    (event.command == Mojo.Menu.helpCmd)) {
         event.stopPropagation(); // enable help so we can handle it
    }
	if (event.type === Mojo.Event.command && event.command === Mojo.Menu.helpCmd) {
		Mojo.Controller.stageController.popScene();
	}
};
