function Bet(player) {
	var amount = player.takeBet();
	this.starting = amount;
	this.current = amount;
}

Bet.prototype.doubleDown = function(player) {
	if (this.starting != player.betAmount) {
		throw 'The player betAmount was altered since the bet was placed.';
	}
	this.current += player.takeBet();
};

Bet.prototype.surrender = function(player) {
    var refund = this.current / 2; //refund half of the bet
    player.updateMoney(refund); 
    return refund;
};


function BettingBox(player) {
	this.player = player;
	this.bets = [];
}

BettingBox.prototype.canPlayerAffordDouble = function(handIndex) {
    var canAfford = false;
    if (handIndex < this.bets.length) {
        canAfford = this.bets[handIndex].starting <= this.player.money;
    }
    return canAfford;
};

BettingBox.prototype.newRound = function() {
	this.bets.length = 0;
	this.bets.push(new Bet(this.player));
};

// used when a player splits
BettingBox.prototype.addBet = function() {
	this.bets.push(new Bet(this.player));
};

// pays out result of hand
BettingBox.prototype.payout = function(results) {
	if (this.bets.length != results.length) {
		throw 'the number of hand results does not match the number of bets';
	}
	var adjustment = 0; // the amount to give back to the player
    var totalWagered = 0;
	for (var i = 0; i < results.length; i++) {
		var result = results[i];
        var currentBet = this.bets[i].current;
        totalWagered += currentBet;
		if (result.winner == 'player') { // pay winnings
			adjustment += (result.betMultiplier + 1) * currentBet;
		} else if (result.winner == 'push') { // refund bet
			adjustment += currentBet;
		}
		// if player lost, don't do anything, we already took their money
	}
	if (adjustment !== 0) {
		this.player.updateMoney(adjustment);
	}
    return adjustment - totalWagered;
};

BettingBox.prototype.doubleDown = function(handIndex) {
    this.bets[handIndex].doubleDown(this.player);
};

BettingBox.prototype.surrender = function(handIndex) {
    return this.bets[handIndex].surrender(this.player);
};
