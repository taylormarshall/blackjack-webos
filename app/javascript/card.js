function Card(suit, rank) {
	this.suit = suit;
	this.rank = rank;
	this.value = parseInt(rank, 10);
    if (isNaN(this.value)) {
        this.value = rank === 'a' ? 1 : 10;
    }
	this.htmlCache = null;
}

Card.prototype.TEMPLATE = new Template('<img class="card" src="images/cards/#{suit}-#{rank}-75.png" alt="card"/>');

Card.prototype.getHtml = function() {
	if (this.htmlCache === null) {
		this.htmlCache = this.TEMPLATE.evaluate(this);
	}
	return this.htmlCache;
};

var HOLECARD_IMG_SRC = 'images/cards/back-#{color}-75-3.png';
var HOLECARD_TEMPLATE = new Template('<img id="holeCard" class="card" src="'+ HOLECARD_IMG_SRC + '" alt="card"/>');
