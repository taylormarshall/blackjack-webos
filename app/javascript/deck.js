var SUITS = ['spades', 'diamonds', 'clubs', 'hearts'];
var RANKS = ['a', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'j', 'q', 'k'];

// builds a deck of cards and keeps track of draw/discard/shuffle logic
function Deck(numberOfDecks) {
    this.numberOfDecks = numberOfDecks || 1;
    this.cardsInPlay = [];
    this.discardPile = [];
    this.buildDrawPile();
	this.shuffle();
}

// creates a new deck of cards (should only be called by Deck)
Deck.prototype.buildDrawPile = function() {
    var i, r, s = SUITS.length, deck = [];

    // create a single deck
    while (s--) {
        r = RANKS.length;
        while (r--) {
            deck.push(new Card(SUITS[s], RANKS[r]));
        }
    }

    // create an entry for all the cards for each deck we want
    this.drawPile = [];
    i = this.numberOfDecks;
    while (i--) {
        this.drawPile = this.drawPile.concat(deck);
    }
};

// Shuffles the cards in the draw pile (should only be called by Deck)
Deck.prototype.shuffle = function() {
    var n = this.drawPile.length;
    while (n > 1) {
        n--;
        var k = Math.floor((n + 1) * Math.random());
        var temp = this.drawPile[k];
        this.drawPile[k] = this.drawPile[n];
        this.drawPile[n] = temp;
    }
};

// draw a new card from the deck, grabbing discard pile and shuffling if necessary
Deck.prototype.draw = function() {
	if (this.drawPile.length < 1) { // TODO let player know about the shuffle
		this.drawPile = this.discardPile;
		this.discardPile = [];
		this.shuffle();
	}
	var drawnCard = this.drawPile.pop();
	this.cardsInPlay.push(drawnCard);
	return drawnCard;
};

// puts all the cards in play into the discard pile (should only be used by Round constructor)
Deck.prototype.newRound = function() {
	this.discardPile = this.discardPile.concat(this.cardsInPlay);
	this.cardsInPlay = [];

    if (SETTINGS && SETTINGS.numberOfDecks !== this.numberOfDecks) {
        // user has changed their settings
        // it's a new round, we know that no cards are in play
        // we can safely rebuild the deck
        this.numberOfDecks = SETTINGS.numberOfDecks;
        this.buildDrawPile();
        this.discardPile = [];
        this.shuffle();
    }
};

