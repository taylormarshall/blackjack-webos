// start a new hand by drawing 2 cards
function Hand(deck, card) {
	this.deck = deck;
	if (card === undefined) {
		this.cards = [deck.draw(), deck.draw()];
	} else {
		this.cards = [card];
	}
	this.isDoubledDown = false;
}

Hand.prototype.isSplittable = function() {
	return this.cards.length === 2 && this.cards[0].value == this.cards[1].value;
};

Hand.prototype.hit = function() {
	this.cards.push(this.deck.draw());
};

Hand.prototype.doubleDown = function() {
	// TODO put the restriction on player money here? maybe in Round?
	if (this.isDoubledDown) {
		throw 'Hand is already doubled down, cannot double down again.';
	}
	this.hit();
	this.isDoubledDown = true;
};

Hand.prototype.getScore = function() {
    var total = 0;
    var aceCount = 0;
    for (var i = 0; i < this.cards.length; i++) {
        if (this.cards[i].rank === 'a') {
            aceCount++;
        } else {
            total += this.cards[i].value;
        }
    }
    if (aceCount > 0) {
        // you could never use two aces as 11 without a bust
        // so always count all but one ace as 1 point
        total += aceCount - 1;
        if (total + 11 <= 21) {
            total += 11;
        } else {
            total++; // using an ace as 11 would cause a bust, use it as 1 
        }
    }
    return total;
};

Hand.prototype.isBust = function() {
	return this.getScore() > 21;
};

Hand.prototype.isBlackjack = function() {
	return this.cards.length === 2 && this.getScore() == 21;
};
