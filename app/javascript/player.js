var MINIMUM_BET = 10;

function Player() {
	this.cookie = new Mojo.Model.Cookie('player');
	var existing = this.cookie.get();
	if (existing && existing.money > MINIMUM_BET) {
		this.money = existing.money;
		this.originalBet = Math.min(existing.originalBet, this.money);
		this.betAmount = this.originalBet;
	} else {
		this.reset();
	}
}

Player.prototype.save = function() {
	this.cookie.put({ originalBet: this.originalBet, money: this.money });
};

Player.prototype.changeBet = function(amount) {
	var newBet = Math.min(amount, this.money);
	this.originalBet = newBet;
	this.betAmount = newBet;
	this.save();
};

Player.prototype.updateMoney = function(change) {
	this.money += change;
	this.save();
};

Player.prototype.setMoney = function(amount) {
	this.money = amount;
	this.save();
};

Player.prototype.takeBet = function() {
	if (this.betAmount > this.money) {
		throw 'Player does not have enough money to make that bet.';
	}
	this.money -= this.betAmount;
	this.save();
	return this.betAmount;
};

// set to the state of a brand new game
Player.prototype.reset = function() {
	this.originalBet = MINIMUM_BET;
	this.money = 500;
	this.betAmount = this.originalBet;
	this.save();
};
