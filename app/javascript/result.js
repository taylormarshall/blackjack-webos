function RoundResult(playerHand, dealerHand) {
	function getHandResult(hand) {
		var score = hand.getScore();
		return {
			score: score,
			isBlackjack: score === 21 && hand.cards.length === 2,
			isBust: score > 21
		};
	}
	var playerResult = getHandResult(playerHand);
	var dealerResult = getHandResult(dealerHand);
	this.playerScore = playerResult.score;
	this.dealerScore = dealerResult.score;

	if (playerResult.isBust || (!dealerResult.isBust && this.dealerScore > this.playerScore)) {
		this.winner = 'dealer';
		this.loserBusted = playerResult.isBust;
		this.winnerBlackjack = dealerResult.isBlackjack;
	} else if (dealerResult.isBust || this.playerScore > this.dealerScore) {
		this.winner = 'player';
		this.loserBusted = dealerResult.isBust;
		this.winnerBlackjack = playerResult.isBlackjack;
	} else if (playerResult.isBlackjack === dealerResult.isBlackjack) {
		this.winner = 'push';
		this.loserBusted = false;
		this.winnerBlackjack = false;
	} else {
		// by now we know exactly 1 has a blackjack
		this.winner = playerResult.isBlackjack ? 'player' : 'dealer';
		this.loserBusted = false;
		this.winnerBlackjack = true;
	}
	
	if (playerResult.isBlackjack && this.winner === 'player') {
		this.betMultiplier = 1.5;
	} else {
		this.betMultiplier = 1;
	}
}
