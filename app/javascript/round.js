//TODO consider a Dealer object that has the dealerHand and makes decision on dealers behalf
function Round(deck) {
	deck.newRound();
	this.deck = deck;
	this.currentPlayerHand = new Hand(deck);
	this.currentPlayerHandIndex = 0; 
	this.playerHands = [this.currentPlayerHand];
	this.dealerHand = new Hand(deck);
	this.isHoleCardCovered = true;
}

Round.prototype.hasPlayerSplit = function() {
	return this.playerHands.length > 1;
};

Round.prototype.canPlayerSplit = function() {
	//only allow splitting to 4 hands
	return this.playerHands.length < 4 && this.currentPlayerHand.isSplittable();
};

Round.prototype.canPlayerDoubleDown = function() {
	// do not allow double after split
	return !this.hasPlayerSplit() && this.currentPlayerHand.cards.length == 2; 
};

Round.prototype.canPlayerSurrender = function() {
	return this.playerHands.length == 1 && this.currentPlayerHand.cards.length == 2;
};

Round.prototype.playerHit = function() {
	this.currentPlayerHand.hit();	
};

Round.prototype.playerDoubleDown = function() {
	this.currentPlayerHand.doubleDown();	
};

Round.prototype.isDealerFinished = function() {
	var isFinished = false;
	if (this.playerHandsAllBlackjack()) {
		var upcard = this.dealerHand.cards[0];
		if (this.isHoleCardCovered) {
			// finish without reveal if dealer can't possibly have a blackjack
			isFinished = upcard.value != 10 && upcard.rank != 'a'; 
		} else {
			// holecard already revealed, in any case we should stop
			isFinished = true; 
		}
	} else {
		// dealer stands on soft 17
		isFinished = !this.isHoleCardCovered && this.dealerHand.getScore() >= 17;
		// dealer doesn't reveal his card if the player busted on all hands
		isFinished |= this.playerHandsAllBust();
	}
	return isFinished;
};

Round.prototype.doNextDealerAction = function() {
	if (this.isHoleCardCovered) {
		// first, just reveal the hole card
		this.isHoleCardCovered = false;
	} else {
		this.dealerHand.hit();
	}
};

Round.prototype.playerSplit = function() {
	var cardToMove = this.currentPlayerHand.cards.pop();
	var nextHand = new Hand(this.deck, cardToMove);
	this.currentPlayerHand.hit();
	nextHand.hit();
	this.playerHands.push(nextHand);
};

Round.prototype.isPlayerFinished = function() {
	return this.currentPlayerHandIndex >= this.playerHands.length;
};

Round.prototype.playerStand = function() {
	this.currentPlayerHandIndex++;
	if (this.currentPlayerHandIndex < this.playerHands.length) {
		this.currentPlayerHand = this.playerHands[this.currentPlayerHandIndex];
	}
};

Round.prototype.playerHandsAllBust = function() {
	return this.playerHandsAllMeetCondition(function(hand) { 
		return hand.isBust();
	});
};

Round.prototype.playerHandsAllBlackjack = function() {
	return this.playerHandsAllMeetCondition(function(hand) { 
		return hand.isBlackjack();
	});
};

Round.prototype.playerHandsAllMeetCondition = function(conditionFunc) {
	var allMet = this.playerHands.length > 0;
	for (var i = 0; i < this.playerHands.length; i++) {
		if (!conditionFunc(this.playerHands[i])) {
			allMet = false;
			break;
		}
	}
	return allMet;
};

Round.prototype.getPlayerPoints = function() {
	return this.currentPlayerHand.getScore();
};

Round.prototype.getDealerPoints = function() {
	return this.dealerHand.getScore();
};

Round.prototype.getOutcomes = function() {
	var outcomes = [];
	for (var i = 0; i < this.playerHands.length; i++) {
		outcomes.push(new RoundResult(this.playerHands[i], this.dealerHand));
	}
	return outcomes;
};
