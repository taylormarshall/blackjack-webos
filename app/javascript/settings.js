function Settings() {
	this.cookie = new Mojo.Model.Cookie('settings');
	var existing = this.cookie.get();
	if (existing) {
        this.soundEnabled = existing.soundEnabled || false;
        this.cardColor = existing.cardColor || 'blue';
        this.numberOfDecks = existing.numberOfDecks || 1;
	} else {
		this.reset();
	}
}

Settings.prototype.save = function() {
	this.cookie.put({
		soundEnabled: this.soundEnabled,
		cardColor: this.cardColor,
		numberOfDecks: this.numberOfDecks
	});
};

// set to the state of newly installed game
Settings.prototype.reset = function() {
    this.soundEnabled = true;
    this.cardColor = 'blue';
    this.numberOfDecks = 1;
	this.save();
};

var SETTINGS = new Settings(); // global settings var created on app load
