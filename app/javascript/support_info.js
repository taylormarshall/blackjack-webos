_APP_Name = 'Blackjack';
_APP_VersionNumber = Mojo.appInfo.version;
_APP_PublisherName = 'Taylor Marshall';
_APP_Copyright = '&copy; Copyright 2009 Taylor Marshall';
_APP_Support_URL = 'http://taylormarshall.info/blackjack';   // label = �Support Website�
_APP_Support_Email = {
	address: 'blackjack@taylormarshall.info',
	subject: 'Support'
};        // label = �Send Email�
_APP_Help_Resource = [
	{ type: 'web', label: 'About Blackjack (Wikipedia)', url: 'http://en.wikipedia.org/wiki/Blackjack'},
	{ type : 'scene', label: 'House Rules', sceneName: 'rules' }
];