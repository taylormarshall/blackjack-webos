function MockPlayer(betAmount) {
	this.betAmount = betAmount;
	this.takeBetCalled = false;
    this.amountUpdated = 0;
}

MockPlayer.prototype.takeBet = function() {
	this.takeBetCalled = true;
	return this.betAmount;
};

MockPlayer.prototype.updateMoney = function(amount) {
    this.amountUpdated = amount;
};

function BettingTests() {}

BettingTests.prototype.testBettingBoxConstructor = function() {
	var bettingBox = new BettingBox(new MockPlayer());
	Mojo.requireArray(bettingBox.bets);
	Mojo.requireEqual(0, bettingBox.bets.length);
	Mojo.require(bettingBox.player instanceof MockPlayer);
	return Mojo.Test.passed;
};

BettingTests.prototype.testBetConstructor = function() {
	var mockPlayer = new MockPlayer(10);
	var bet = new Bet(mockPlayer);
	Mojo.requireEqual(10, bet.starting);
	Mojo.requireEqual(10, bet.current);
	Mojo.require(mockPlayer.takeBetCalled, 'takeBet should have been called on player');
	return Mojo.Test.passed;
};

BettingTests.prototype.testBettingBoxNewRound = function() {
	var mockPlayer = new MockPlayer();
	var bettingBox = new BettingBox(mockPlayer);
	bettingBox.newRound();
	Mojo.requireEqual(1, bettingBox.bets.length);
	Mojo.require(bettingBox.bets[0] instanceof Bet);
	return Mojo.Test.passed;
};

BettingTests.prototype.testBettingBoxAddBet = function() {
	var mockPlayer = new MockPlayer();
	var bettingBox = new BettingBox(mockPlayer);
	bettingBox.newRound();
	bettingBox.addBet();
	Mojo.requireEqual(2, bettingBox.bets.length);
	Mojo.require(bettingBox.bets[1] instanceof Bet);
	bettingBox.addBet();
	Mojo.requireEqual(3, bettingBox.bets.length);
	Mojo.require(bettingBox.bets[2] instanceof Bet);
	return Mojo.Test.passed;
};

BettingTests.prototype.testBettingBoxPayout = function() {
	var mockPlayer = new MockPlayer(10);
    var bettingBox = new BettingBox(mockPlayer);
    bettingBox.newRound();
    var results = [{ winner: 'player', betMultiplier: 1 }];
    Mojo.requireEqual(10, bettingBox.payout(results));
    Mojo.requireEqual(20, mockPlayer.amountUpdated);//bet + winnings
    mockPlayer.amountUpdated = 0;
    results = [{ winner: 'push', betMultiplier: 1 }];
    Mojo.requireEqual(0, bettingBox.payout(results));
    Mojo.requireEqual(10, mockPlayer.amountUpdated);
    mockPlayer.amountUpdated = 0;
    bettingBox.addBet();
    results = [{ winner: 'player', betMultiplier: 1 }, { winner: 'push', betMultiplier: 1 }];
    Mojo.requireEqual(10, bettingBox.payout(results));
    Mojo.requireEqual(30, mockPlayer.amountUpdated);
    mockPlayer.amountUpdated = 0;
    results = [{ winner: 'dealer', betMultiplier: 1 }, { winner: 'dealer', betMultiplier: 1 }];
    Mojo.requireEqual(-20, bettingBox.payout(results));
    Mojo.requireEqual(0, mockPlayer.amountUpdated);
	return Mojo.Test.passed;
};

BettingTests.prototype.testBetDoubleDown = function() {
	var mockPlayer = new MockPlayer(10);
	var bet = new Bet(mockPlayer);
	mockPlayer.takeBetCalled = false;
	bet.doubleDown(mockPlayer);
	Mojo.require(mockPlayer.takeBetCalled, 'takeBet should have been called on player');
	Mojo.requireEqual(10, bet.starting);
	Mojo.requireEqual(20, bet.current);
	mockPlayer.betAmount = 15;
	try {
		bet.doubleDown(mockPlayer);
		return 'exception should have been thrown since bet was altered';
	} catch(e) {
		Mojo.requireEqual('The player betAmount was altered since the bet was placed.', e);
	}
	return Mojo.Test.passed;
};
