function CardTests() {}

CardTests.prototype.testConstructor = function() {
	var aceOfHearts = new Card('hearts', 'a');
	Mojo.requireEqual('a', aceOfHearts.rank);
	Mojo.requireEqual('hearts', aceOfHearts.suit);
	Mojo.requireNumber(aceOfHearts.value);
	Mojo.requireEqual(1, aceOfHearts.value);
	Mojo.requireEqual('<img class="card" src="images/cards/hearts-a-75.png" alt="card"/>', aceOfHearts.getHtml());
	
	var jackOfClubs = new Card('clubs', 'j');
	Mojo.requireEqual('j', jackOfClubs.rank);
	Mojo.requireEqual('clubs', jackOfClubs.suit);
	Mojo.requireNumber(jackOfClubs.value);
	Mojo.requireEqual(10, jackOfClubs.value);
	Mojo.requireEqual('<img class="card" src="images/cards/clubs-j-75.png" alt="card"/>', jackOfClubs.getHtml());
	
	var queenOfDiamonds = new Card('diamonds', 'q');
	Mojo.requireEqual('q', queenOfDiamonds.rank);
	Mojo.requireEqual('diamonds', queenOfDiamonds.suit);
	Mojo.requireNumber(queenOfDiamonds.value);
	Mojo.requireEqual(10, queenOfDiamonds.value);
	Mojo.requireEqual('<img class="card" src="images/cards/diamonds-q-75.png" alt="card"/>', queenOfDiamonds.getHtml());
	
	var kingOfSpades = new Card('spades', 'k');
	Mojo.requireEqual('k', kingOfSpades.rank);
	Mojo.requireEqual('spades', kingOfSpades.suit);
	Mojo.requireNumber(kingOfSpades.value);
	Mojo.requireEqual(10, kingOfSpades.value);
	Mojo.requireEqual('<img class="card" src="images/cards/spades-k-75.png" alt="card"/>', kingOfSpades.getHtml());
	
	var three = new Card('spades', '3');
	Mojo.requireEqual('spades', three.suit);
	Mojo.requireEqual('3', three.rank);
	Mojo.requireNumber(three.value);
	Mojo.requireEqual(3, three.value);
	Mojo.requireEqual('<img class="card" src="images/cards/spades-3-75.png" alt="card"/>', three.getHtml());
	
	var six = new Card('clubs', '6');
	Mojo.requireEqual('clubs', six.suit);
	Mojo.requireEqual('6', six.rank);
	Mojo.requireNumber(six.value);
	Mojo.requireEqual(6, six.value);
	Mojo.requireEqual('<img class="card" src="images/cards/clubs-6-75.png" alt="card"/>', six.getHtml());
	
	var nine = new Card('hearts', '9');
	Mojo.requireEqual('hearts', nine.suit);
	Mojo.requireEqual('9',nine.rank);
	Mojo.requireNumber(nine.value);
	Mojo.requireEqual(9, nine.value);
	Mojo.requireEqual('<img class="card" src="images/cards/hearts-9-75.png" alt="card"/>', nine.getHtml());
	
	return Mojo.Test.passed;
};
