function MockDeck(cards) {
	this.cards = cards;	
	this.cards_drawn = 0;
	this.new_round = true;
}

MockDeck.prototype.draw = function() {
	this.cards_drawn++;
	if (this.cards === undefined || this.cards.length === 0) {
		return { value: 1 };
	} else {
		return this.cards.pop();
	}
};

MockDeck.prototype.newRound = function() {
	this.new_round = true;
};

function HandTests() {}

HandTests.prototype.testConstructor = function() {
	var mockDeck = new MockDeck();
	var hand = new Hand(mockDeck);
	Mojo.requireFalse(hand.isDoubledDown);
	Mojo.requireEqual(2, hand.deck.cards_drawn);
	Mojo.requireEqual(2, hand.cards.length);
	
	mockDeck = new MockDeck();
	var splitHand = new Hand(mockDeck, { value: 1});
	Mojo.requireFalse(splitHand.isDoubledDown);
	Mojo.requireEqual(0, splitHand.deck.cards_drawn);
	Mojo.requireEqual(1, splitHand.cards.length);
	
	return Mojo.Test.passed;
};

HandTests.prototype.testHit = function() {
	var mockDeck = new MockDeck();
	var hand = new Hand(mockDeck);
	Mojo.requireEqual(2, hand.deck.cards_drawn);
	Mojo.requireEqual(2, hand.cards.length);
	
	hand.hit();
	Mojo.requireEqual(3, hand.deck.cards_drawn);
	Mojo.requireEqual(3, hand.cards.length);
	Mojo.requireFalse(hand.isDoubledDown);
	
	hand.hit();
	Mojo.requireEqual(4, hand.deck.cards_drawn);
	Mojo.requireEqual(4, hand.cards.length);
	Mojo.requireFalse(hand.isDoubledDown);
	
	return Mojo.Test.passed;
};

HandTests.prototype.testDoubleDown = function() {
	var mockDeck = new MockDeck();
	var hand = new Hand(mockDeck);
	Mojo.requireEqual(2, hand.deck.cards_drawn);
	Mojo.requireEqual(2, hand.cards.length);
	
	hand.doubleDown();
	Mojo.requireEqual(3, hand.deck.cards_drawn);
	Mojo.requireEqual(3, hand.cards.length);
	Mojo.require(hand.isDoubledDown);
	
	try {
		hand.doubleDown();
		return 'Should not have been able to double down twice';
	} catch (e) {
		Mojo.requireEqual('Hand is already doubled down, cannot double down again.', e);
	}
	
	return Mojo.Test.passed;
};

HandTests.prototype.testIsSplittable = function() {
	var mockDeck = new MockDeck([{value:1}, {value:1}]);
	var hand = new Hand(mockDeck);
	Mojo.require(hand.isSplittable());
	
	mockDeck = new MockDeck([{value:3}, {value:8}]);
	hand = new Hand(mockDeck);
	Mojo.requireFalse(hand.isSplittable());

	
	mockDeck = new MockDeck([{value:1}, {value:1}, {value:1}]);
	hand = new Hand(mockDeck);
	hand.hit();
	Mojo.requireFalse(hand.isSplittable());
	
	return Mojo.Test.passed;
};

HandTests.prototype.testGetScore = function() {
	var mockDeck = new MockDeck([{rank: 'a'}, {value: 10}]);
	var blackjackHand = new Hand(mockDeck);
	Mojo.requireEqual(21, blackjackHand.getScore());
	
	mockDeck = new MockDeck([{rank: 'a'}, {value: 5}]);
	var aceHand11 = new Hand(mockDeck);
	Mojo.requireEqual(16, aceHand11.getScore());
	
	mockDeck = new MockDeck([{rank: 'a'}, {value: 7}, {value: 10}]);
	var aceHand1 = new Hand(mockDeck);
	aceHand1.hit();
	Mojo.requireEqual(18, aceHand1.getScore());
	
	mockDeck = new MockDeck([{rank: 'a'}, {rank: 'a'}, {value: 5}]);
	var twoAceHand = new Hand(mockDeck);
	twoAceHand.hit();
	Mojo.requireEqual(17, twoAceHand.getScore());
	
	mockDeck = new MockDeck([{rank: 'a'}, {rank: 'a'}, {value: 10}]);
	var twoAceLowHand = new Hand(mockDeck);
	twoAceLowHand.hit();
	Mojo.requireEqual(12, twoAceLowHand.getScore());
	
	mockDeck = new MockDeck([{value: 10}, {value: 2}, {value: 10}]);
	var bustHand = new Hand(mockDeck);
	bustHand.hit();
	Mojo.requireEqual(22, bustHand.getScore());
	
	mockDeck = new MockDeck([{value: 10}, {value: 10}, {value: 10}]);
	bustHand = new Hand(mockDeck);
	bustHand.hit();
	Mojo.requireEqual(30, bustHand.getScore());
	
	mockDeck = new MockDeck([{value: 10}, {value: 10}, {rank: 'a'}]);
	var oneAceHand21 = new Hand(mockDeck);
	oneAceHand21.hit();
	Mojo.requireEqual(21, oneAceHand21.getScore());
	
	return Mojo.Test.passed;
};

HandTests.prototype.testIsBust = function() {
	var hand = new Hand(new MockDeck());
	
	hand.getScore = function() { return 3; };
	Mojo.requireFalse(hand.isBust());
	
	hand.getScore = function() { return 11; };
	Mojo.requireFalse(hand.isBust());
	
	hand.getScore = function() { return 21; };
	Mojo.requireFalse(hand.isBust());
	
	hand.getScore = function() { return 22; };
	Mojo.require(hand.isBust());
	
	hand.getScore = function() { return 30; };
	Mojo.require(hand.isBust());
	
	return Mojo.Test.passed;
};

HandTests.prototype.testIsBlackjack = function() {
	var hand = new Hand(new MockDeck());
	
	hand.getScore = function() { return 3; };
	Mojo.requireFalse(hand.isBlackjack());
	
	hand.getScore = function() { return 11; };
	Mojo.requireFalse(hand.isBlackjack());
	
	hand.getScore = function() { return 21; };
	Mojo.require(hand.isBlackjack());
	
	hand.getScore = function() { return 22; };
	Mojo.requireFalse(hand.isBlackjack());
	
	hand.getScore = function() { return 30; };
	Mojo.requireFalse(hand.isBlackjack());
		
	hand.hit();
	hand.getScore = function() { return 21; };
	Mojo.requireFalse(hand.isBlackjack());
	
	return Mojo.Test.passed;
};
