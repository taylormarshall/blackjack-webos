function ResultTests() {}

ResultTests.prototype.testRoundResult_simple = function() {
	var mockDeck = new MockDeck();
	var round = new Round(mockDeck);
	var mockDoubledHand = new MockHand(18);
	mockDoubledHand.isDoubledDown = true;
	var mockBlackjackHand = new MockHand(21);
	mockBlackjackHand.blackjack = true;
    mockBlackjackHand.cards = [0,0];//dummy cards length = 2
	var mockMultiCard21 = new MockHand(21);
	var mock17Hand = new MockHand(17);
	var mock19Hand = new MockHand(19);
	
	round.playerHands = [mockMultiCard21, mock17Hand, mockDoubledHand, mockBlackjackHand];
	
	round.dealerHand = new MockHand(20);	
	var outcome = new RoundResult(round.playerHands[0], round.dealerHand);
	Mojo.requireEqual('player', outcome.winner);
	Mojo.requireEqual(1, outcome.betMultiplier);
	
	round.dealerHand = new MockHand(18);	
	outcome = new RoundResult(round.playerHands[1], round.dealerHand);
	Mojo.requireEqual('dealer', outcome.winner);
	Mojo.requireEqual(1, outcome.betMultiplier);
	
	round.dealerHand = mock17Hand;	
	outcome = new RoundResult(round.playerHands[1], round.dealerHand);
	Mojo.requireEqual('push', outcome.winner);
	Mojo.requireEqual(1, outcome.betMultiplier);
	
	outcome = new RoundResult(round.playerHands[2], round.dealerHand);
	Mojo.requireEqual('player', outcome.winner);
	// bet multipler should still be 1 in this case
	// the actual BET should have increased for double down or split
	Mojo.requireEqual(1, outcome.betMultiplier);
	
	outcome = new RoundResult(round.playerHands[3], round.dealerHand);
	Mojo.requireEqual('player', outcome.winner);
	Mojo.requireEqual(1.5, outcome.betMultiplier);
	
	round.dealerHand = mockBlackjackHand;
	outcome = new RoundResult(round.playerHands[3], round.dealerHand);
	Mojo.requireEqual('push', outcome.winner);
	Mojo.requireEqual(1, outcome.betMultiplier);
	
	round.dealerHand = mockMultiCard21;
	outcome = new RoundResult(round.playerHands[3], round.dealerHand);
	Mojo.requireEqual('player', outcome.winner, 'player blackjack should beat multicard 21');
	Mojo.requireEqual(1.5, outcome.betMultiplier);
	
	round.dealerHand = mockBlackjackHand;
	outcome = new RoundResult(round.playerHands[0], round.dealerHand);
	Mojo.requireEqual('dealer', outcome.winner, 'dealer blackjack should beat multicard 21');
	Mojo.requireEqual(1, outcome.betMultiplier);
	
	round.dealerHand = mockMultiCard21;
	outcome = new RoundResult(round.playerHands[0], round.dealerHand);
	Mojo.requireEqual('push', outcome.winner);
	Mojo.requireEqual(1, outcome.betMultiplier);
	
	return Mojo.Test.passed;
};

ResultTests.prototype.testRoundResult_complex = function() {
	var round = new Round(new MockDeck());
	round.dealerHand = testHand('a', '5', '5');
	round.playerHands[0] = testHand('a', 'j');
	var outcome = new RoundResult(round.playerHands[0], round.dealerHand);
	Mojo.requireEqual('player', outcome.winner);
	Mojo.requireEqual(1.5, outcome.betMultiplier);
	Mojo.require(outcome.winnerBlackjack);
	Mojo.requireFalse(outcome.loserBusted);
	Mojo.requireEqual(21, outcome.playerScore);
	Mojo.requireEqual(21, outcome.dealerScore);	
	
	round.dealerHand = testHand('a', '3', '7');
	round.playerHands[0] = testHand('a', '8', '2');
	outcome = new RoundResult(round.playerHands[0], round.dealerHand);
	Mojo.requireEqual('push', outcome.winner);
	Mojo.requireEqual(1, outcome.betMultiplier);
	Mojo.requireFalse(outcome.winnerBlackjack);
	Mojo.requireFalse(outcome.loserBusted);
	Mojo.requireEqual(21, outcome.playerScore);
	Mojo.requireEqual(21, outcome.dealerScore);
	
	round.dealerHand = testHand('a', 'q');
	round.playerHands[0] = testHand('a', 'k');
	outcome = new RoundResult(round.playerHands[0], round.dealerHand);
	Mojo.requireEqual('push', outcome.winner);
	Mojo.requireEqual(1, outcome.betMultiplier);
	Mojo.requireFalse(outcome.winnerBlackjack);
	Mojo.requireFalse(outcome.loserBusted);
	Mojo.requireEqual(21, outcome.playerScore);
	Mojo.requireEqual(21, outcome.dealerScore);
	
	round.dealerHand = testHand('q', 'q', '5');
	round.playerHands[0] = testHand('q', 'k', '7');
	outcome = new RoundResult(round.playerHands[0], round.dealerHand);
	Mojo.requireEqual('dealer', outcome.winner);
	Mojo.requireEqual(1, outcome.betMultiplier);
	Mojo.requireFalse(outcome.winnerBlackjack);
	Mojo.require(outcome.loserBusted);
	Mojo.requireEqual(27, outcome.playerScore);
	Mojo.requireEqual(25, outcome.dealerScore);
	
	round.dealerHand = testHand('a', '7');
	round.playerHands[0] = testHand('a', '8', '2');
	outcome = new RoundResult(round.playerHands[0], round.dealerHand);
	Mojo.requireEqual('player', outcome.winner);
	Mojo.requireEqual(1, outcome.betMultiplier);
	Mojo.requireFalse(outcome.winnerBlackjack);
	Mojo.requireFalse(outcome.loserBusted);
	Mojo.requireEqual(21, outcome.playerScore);
	Mojo.requireEqual(18, outcome.dealerScore);
	
	round.dealerHand = testHand('a', 'k');
	round.playerHands[0] = testHand('a', '8', '2');
	outcome = new RoundResult(round.playerHands[0], round.dealerHand);
	Mojo.requireEqual('dealer', outcome.winner);
	Mojo.requireEqual(1, outcome.betMultiplier);
	Mojo.require(outcome.winnerBlackjack);
	Mojo.requireFalse(outcome.loserBusted);
	Mojo.requireEqual(21, outcome.playerScore);
	Mojo.requireEqual(21, outcome.dealerScore);
	
	round.dealerHand = testHand('a', 'k');
	round.playerHands[0] = testHand('q', '7');
	outcome = new RoundResult(round.playerHands[0], round.dealerHand);
	Mojo.requireEqual('dealer', outcome.winner);
	Mojo.requireEqual(1, outcome.betMultiplier);
	Mojo.require(outcome.winnerBlackjack);
	Mojo.requireFalse(outcome.loserBusted);
	Mojo.requireEqual(17, outcome.playerScore);
	Mojo.requireEqual(21, outcome.dealerScore);
	
	round.dealerHand = testHand('a', 'k');
	round.playerHands[0] = testHand('q', '7');
	outcome = new RoundResult(round.playerHands[0], round.dealerHand);
	Mojo.requireEqual('dealer', outcome.winner);
	Mojo.requireEqual(1, outcome.betMultiplier);
	Mojo.require(outcome.winnerBlackjack);
	Mojo.requireFalse(outcome.loserBusted);
	Mojo.requireEqual(17, outcome.playerScore);
	Mojo.requireEqual(21, outcome.dealerScore);
	
	round.dealerHand = testHand('6', '6', 'k');
	round.playerHands[0] = testHand('4', '7');
	outcome = new RoundResult(round.playerHands[0], round.dealerHand);
	Mojo.requireEqual('player', outcome.winner);
	Mojo.requireEqual(1, outcome.betMultiplier);
	Mojo.requireFalse(outcome.winnerBlackjack);
	Mojo.require(outcome.loserBusted);
	Mojo.requireEqual(11, outcome.playerScore);
	Mojo.requireEqual(22, outcome.dealerScore);
	
	round.dealerHand = testHand('7', '3', '7');
	round.playerHands[0] = testHand('10', '5', 'q');
	outcome = new RoundResult(round.playerHands[0], round.dealerHand);
	Mojo.requireEqual('dealer', outcome.winner);
	Mojo.requireEqual(1, outcome.betMultiplier);
	Mojo.requireFalse(outcome.winnerBlackjack);
	Mojo.require(outcome.loserBusted);
	Mojo.requireEqual(25, outcome.playerScore);
	Mojo.requireEqual(17, outcome.dealerScore);
	
	round.dealerHand = testHand('7', '3', '7');
	round.playerHands[0] = testHand('10', '7');
	outcome = new RoundResult(round.playerHands[0], round.dealerHand);
	Mojo.requireEqual('push', outcome.winner);
	Mojo.requireEqual(1, outcome.betMultiplier);
	Mojo.requireFalse(outcome.winnerBlackjack);
	Mojo.requireFalse(outcome.loserBusted);
	Mojo.requireEqual(17, outcome.playerScore);
	Mojo.requireEqual(17, outcome.dealerScore);
	
	return Mojo.Test.passed;
};