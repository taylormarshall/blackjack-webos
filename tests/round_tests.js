function MockHand(score) {
	this.score = score;
	this.blackjack = false;
	this.isDoubledDown = false;
	this.cards = [0,0,0]; // dummy for checking cards length
}

MockHand.prototype.getScore = function() {
	return this.score;
};

MockHand.prototype.isBlackjack = function() {
	return this.blackjack;
};

MockHand.prototype.isBust = function() {
	return this.score > 21;
};

// fills up a hand for testing
function testHand() {
	var ranks = arguments;
	var hand = new Hand(new MockDeck());
	hand.cards = [];
	for (var i = 0; i < ranks.length; i++) {
		hand.cards.push(new Card('spades', ranks[i]));
	}
	return hand;
}

function RoundTests() {}

RoundTests.prototype.testConstructor = function() {
	var mockDeck = new MockDeck();
	mockDeck.new_round = false;
	
	var round = new Round(mockDeck);
	Mojo.require(round.deck instanceof MockDeck);
	Mojo.require(mockDeck.new_round);
	Mojo.require(round.currentPlayerHand instanceof Hand);
	Mojo.require(round.currentPlayerHand.deck instanceof MockDeck);
	Mojo.requireEqual(0, round.currentPlayerHandIndex);
	Mojo.requireArray(round.playerHands);
	Mojo.requireEqual(1, round.playerHands.length);
	Mojo.require(round.dealerHand instanceof Hand);
	Mojo.require(round.dealerHand.deck instanceof MockDeck);
	Mojo.require(round.isHoleCardCovered);
	
	return Mojo.Test.passed;
};

RoundTests.prototype.testConstructor = function() {
	var mockDeck = new MockDeck();
	mockDeck.new_round = false;
	
	var round = new Round(mockDeck);
	Mojo.require(round.deck instanceof MockDeck);
	Mojo.require(mockDeck.new_round);
	Mojo.require(round.currentPlayerHand instanceof Hand);
	Mojo.require(round.currentPlayerHand.deck instanceof MockDeck);
	Mojo.requireEqual(0, round.currentPlayerHandIndex);
	Mojo.requireArray(round.playerHands);
	Mojo.requireEqual(1, round.playerHands.length);
	Mojo.require(round.dealerHand instanceof Hand);
	Mojo.require(round.dealerHand.deck instanceof MockDeck);
	Mojo.require(round.isHoleCardCovered);
	
	return Mojo.Test.passed;
};

RoundTests.prototype.testHasPlayerSplit = function() {
	var mockDeck = new MockDeck();
	var round = new Round(mockDeck);	
	Mojo.requireFalse(round.hasPlayerSplit());
	
	round.playerHands.push(new Hand(mockDeck));
	Mojo.require(round.hasPlayerSplit());
	
	round.playerHands.push(new Hand(mockDeck));
	Mojo.require(round.hasPlayerSplit());

	return Mojo.Test.passed;
};

RoundTests.prototype.testCanPlayerSplit = function() {
	var mockDeck = new MockDeck();
	var round = new Round(mockDeck);
	round.currentPlayerHand.isSplittable = function() { return true; };
	Mojo.require(round.canPlayerSplit());
	
	round.playerHands.push(new Hand(mockDeck));
	Mojo.require(round.canPlayerSplit());
	
	round.playerHands.push(new Hand(mockDeck));
	Mojo.require(round.canPlayerSplit());
	
	round.playerHands.push(new Hand(mockDeck));
	Mojo.requireFalse(round.canPlayerSplit());
	
	round.currentPlayerHand.isSplittable = function() { return false; };
	round.playerHands = [new Hand(mockDeck)];
	Mojo.requireFalse(round.canPlayerSplit());
	
	return Mojo.Test.passed;
};

RoundTests.prototype.testIsDealerFinished = function() {
	var mockDeck = new MockDeck();
	var round = new Round(mockDeck);
	
	// at round creation, holecard is still covered, dealer shouldn't be finished
	round.dealerHand = new MockHand(16);
	Mojo.requireFalse(round.isDealerFinished(), 'should continue since holecard is not revealed');
	
	round.dealerHand = new MockHand(18);
	Mojo.requireFalse(round.isDealerFinished(), 'should continue since holecard is not revealed');
	
	round.dealerHand = new MockHand(21);
	Mojo.requireFalse(round.isDealerFinished(), 'should continue since holecard is not revealed');
	

	round.dealerHand = testHand('4', 'q');
	round.playerHands[0] = testHand('q', '5', '7');
	Mojo.require(round.isDealerFinished(), 'should not reveal holecard if player busted');
	
	round.playerHands[0] = testHand('a', 'k');
	Mojo.require(round.isDealerFinished(), 'should not reveal holecard if player has blackjack and dealer cant have blackjack');
	
	round.dealerHand = testHand('j', '3');
	round.playerHands[0] = testHand('a', 'k');
	Mojo.requireFalse(round.isDealerFinished(), 'should reveal holecard if player has blackjack and dealer can have blackjack');
	
	// after holecard is uncovered, normal rules apply
	round.isHoleCardCovered = false;
	round.playerHands[0] = testHand('q', '8');
	
	round.dealerHand = new MockHand(10);
	Mojo.requireFalse(round.isDealerFinished(), 'hand value was 10, should continue');
	
	round.dealerHand = new MockHand(14);
	Mojo.requireFalse(round.isDealerFinished(), 'hand value was 14, should continue');
	
	round.dealerHand = new MockHand(16);
	Mojo.requireFalse(round.isDealerFinished(), 'hand value was 16, should continue');
	
	round.dealerHand = new MockHand(17);
	Mojo.require(round.isDealerFinished(), 'hand value was 17, should stop');
	
	round.dealerHand = new MockHand(21);
	Mojo.require(round.isDealerFinished(), 'hand value was 21, should stop');
	
	round.dealerHand = testHand('7', '5');
	round.playerHands[0] = testHand('a', 'j');
	Mojo.require(round.isDealerFinished(), 'should not continue if player has blackjack and holecard is revealed');
	
	return Mojo.Test.passed;
};
